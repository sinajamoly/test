import React, { useEffect } from 'react';
import axios from 'axios';
import BaseLayout from '@/Container/BaseLayout/baseLayout';

export default function App() {
    useEffect(() => {
        axios.get('http://127.0.0.1:8000/api/sample').then((response) => {
            console.log(response);
        }).catch((e) => {
            console.log(e);
        })
    }, []);
    return (
        <>
            <BaseLayout/>
        </>
    );
}
